from repositories import Thumbnail


class ThumbnailService:

    def __init__(self):
        pass

    @staticmethod
    def generateThumbnail(video, object_id):
        try:
            thumbnail = Thumbnail(video)
            output = thumbnail.generate_thumbnail(object_id)
        except:
            output = {'image_path': "https://s3-ap-southeast-1.amazonaws.com/ind-ypassets/logos/default_thumbnail.png"}
        return output
