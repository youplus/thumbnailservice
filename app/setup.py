import os
import errno
import boto3
import config

class ModelDownload():
    ml_model_path = os.path.dirname(os.path.realpath(__file__)) + '/MLModel/'

    def __init__(self):
        client = boto3.client("s3",aws_access_key_id=config.aws_access_key_id,aws_secret_access_key=config.aws_secret_access_key,)
        self.download_dir(client, config.bucket_name, config.path_in_bucket, self.ml_model_path)
        print("Download complete!!!\n\n")

    def assert_dir_exists(self,path):

    #Checks if directory tree in path exists. If not it created them.:param path: the path to check if it exists
        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    def download_dir(self,client, bucket, path, target):
        if not path.endswith('/'):
            path += '/'

        paginator = client.get_paginator('list_objects_v2')
        for result in paginator.paginate(Bucket=bucket, Prefix=path):
            # Download each file individually
            for key in result['Contents']:
                # Calculate relative path
                rel_path = key['Key'][len(path):]
                # Skip paths ending in /
                if not key['Key'].endswith('/'):
                    local_file_path = os.path.join(target, rel_path)
                    # Make sure directories exist
                    local_file_dir = os.path.dirname(local_file_path)
                    self.assert_dir_exists(local_file_dir)
                    client.download_file(bucket, key['Key'], local_file_path)
    

if __name__ == "__main__":
    ModelDownload()