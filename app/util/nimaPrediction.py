#imports
import numpy as np
from keras.layers import Dense
from keras.models import Model
from keras.applications.mobilenet import MobileNet
from keras.applications.mobilenet import preprocess_input
from keras.preprocessing.image import load_img, img_to_array

class ThumbnailPrediction:
    """
    Class used to pick thumbnail from a input video frames.
    """

    def __init__(self,path_to_aesthetic_model, path_to_technical_model, graph):
        """Constructor to define parameters for thumbnail prediction from video frames.

        Arguments:
            path_to_aesthetic_model {str} -- absolute path to the aesthetic model.
            path_to_technical_model {str} -- absolute path to the technical model.
        """
        # initialize parameters
        self.img_width = 224
        self.img_height = 224
        self.histogram_range = 10

        # CNN model loading
        # loading aesthetic model
        self.aesthetic_model = self.create_mobile_net()
        self.aesthetic_model.load_weights(path_to_aesthetic_model)

        # loading technical model
        self.technical_model = self.create_mobile_net()
        self.technical_model.load_weights(path_to_technical_model)
        self.graph = graph

    def normalize_labels(self,labels):
        """Normalize the softmax output probabilities.

        Arguments:
            labels {np.array} -- softmax probability array

        Return:
            Normalized softmax probability array
        """

        # make sure the labels are numpy array
        labels_np = np.array(labels)
        # label normalization
        return labels_np / labels_np.sum(axis=1,keepdims=True)

    def calc_mean_score(self,score_dist):
        """Calculates aggregate aesthetic/technical score for an image using normalized softmax probabilities.

        Arguments:
            score_dist {np.array} -- softmax probability array

        Return:
            Weighted(as per annonation score) softmax probabilties as aggregate aesthetic/technical scores
        """
        # normalize the softmax output probabilities
        score_dist = self.normalize_labels(score_dist)

        # creating a weight matrix for probability score
        weight_matrix = np.arange(1,self.histogram_range+1)
        return np.sum(score_dist*weight_matrix,axis=1)

    def create_mobile_net(self):
        """Creates mobilenet architecture.

        Return:
            nima_model {keras model} -- mobilenet architecture with 10 output classes
        """

        # construct mobile architecture fronm keras application
        model = MobileNet(weights="imagenet", include_top=False, input_shape=(self.img_width, self.img_height, 3), pooling="avg")

        # replacing the last dense layer
        output = Dense(self.histogram_range, activation="softmax")(model.output)
        nima_model = Model(model.input,output)
        return nima_model

    def load_image(self, image_path):
        """Loads and pre-processes input image for model prediction.

        Arguments:
            image_path {str} -- absolute path to input image

        Return:
            img {array} -- image array
        """
        # exception if error in image loading
        try :
            # loading the image
            img = img_to_array(load_img(image_path, target_size=(self.img_width, self.img_height, 3)))

            # pre-processing the image
            img = preprocess_input(img)
            return img
        except:
            return False

    def predict_on_batch(self, image_batch):
        """Predicts aesthetic & technical scores for input batch of images.

        Arguments:
            image_batch {list} -- batch of imput images for prediction

        Return:
            predicted_image_paths {list} -- list containing paths to all the images verified for prediction
            nima_scores {numpy-array} -- array containing all the nima-scores for images
            aesthetic_scores {numpy-array} -- array containing all the aesthetic-scores for images
            technical_scores {numpy-array} -- array containing all the technical-scores for images
            False {boolean} -- if the batch of images is an empty array
        """

        # returns False if the batch of image is an empty
        if not image_batch:
            return False

        # initialize parameters
        loaded_image_bank = []
        predicted_image_paths = []

        # Pre-processing images for predictions
        for image_path in image_batch:
            image = self.load_image(image_path)
            if type(image) != bool:
                loaded_image_bank.append(image)
                predicted_image_paths.append(image_path)
        loaded_image_bank = np.array(loaded_image_bank)

        # aesthetic predictions
        with self.graph.as_default():
            aesthetic_preds = self.aesthetic_model.predict(loaded_image_bank)
            aesthetic_scores = self.calc_mean_score(aesthetic_preds)

        # technical predictions
        with self.graph.as_default():
            technical_preds = self.technical_model.predict(loaded_image_bank)
            technical_scores = self.calc_mean_score(technical_preds)

        # harmonic mean of score's
        nima_scores = 2*(aesthetic_scores*technical_scores)/(aesthetic_scores+technical_scores)
        return predicted_image_paths , nima_scores, aesthetic_scores, technical_scores
