#imports
import re
import os
import glob
import shutil
import subprocess
# from moviepy.editor import VideoFileClip


class VideoProcessing:
    """
    Class used to extract and pre-process data from a input video.
    """

    def __init__(self, video_path):
        """Constructor to define parameters for data extraction from video.

        Arguments:
            video_path {str} -- absolute path to the input video file.
        """

        # class variables
        self.video_path = video_path
        self.video_folder , self.video_name = os.path.split(video_path)

        # rename-video (if required)
        if " " in self.video_name:
            self.__rename_video__()

        # create video folder
        self.current_video_folder = os.path.join(self.video_folder,self.video_name.split(".")[0])
        self.__create_folder__(self.current_video_folder,delete=True)

        # create frames folder
        self.frames_folder = os.path.join(self.current_video_folder,"frames")
        self.__create_folder__(self.frames_folder)

        # video meta data
        # self.video_fps = (self.__find_video_metadata__())

    def __rename_video__(self):
        """Renames video if spaces present in video name
        """
        self.video_name = re.sub(' +', ' ', self.video_name)
        self.video_name = self.video_name.replace(" ","_")
        self.video_name = re.sub(r'[\?\!]+(?=[\?\!])', '', self.video_name)

        os.rename(self.video_path,os.path.join(self.video_folder,self.video_name))
        self.video_path = os.path.join(self.video_folder,self.video_name)

    def __create_folder__(self, path_to_folder, delete=False):
        """Creates a system folder if not already present as per the given path.

        Arguments:
            path_to_folder {str} -- absolute path to the folder, to be generated.
            delete {boolean} -- if True will delete existing folder and create an empty folder.
                                No effect otherwise.
        """
        if not os.path.exists(path_to_folder):
            os.mkdir(path_to_folder)
        elif delete:
            shutil.rmtree(path_to_folder)
            os.mkdir(path_to_folder)

    def __find_video_metadata__(self, rotation=False):
        """Extracts metadata for a video using ffmpeg tool.

        Arguments:
            rotation {bool} -- boolean representing whether to detect rotation or not

        Return:
            video_fps {float} -- frame per second of the video
            rotation {int} (optional) -- rotation of frames in the video(landscape/portrait)
        """
        # ffmpeg call for metadata extraction
        cmd = 'ffmpeg -i %s -f null -' % self.video_path
        p = subprocess.Popen(cmd.split(" "), stderr=subprocess.PIPE, close_fds=True)
        stdout, stderr = p.communicate()

        # split ffmpeg result to create python list
        # stderr = re.sub("\\\\n", "", str(stderr)) #(not necessary)
        video_metadata_list = re.split('=|\s+',str(stderr))

        # extract video fps from ffmpeg metadata
        try:
            fps_index = video_metadata_list.index("fps,")
            video_fps = float(video_metadata_list[fps_index + 1])
        except:
            video_fps = -1

        # extract angle of rotation from ffmpeg metadata
        if rotation:
            try:
                rotation_index = video_metadata_list.index("rotate")
                rotation = int(re.split("[^0-9]+", video_metadata_list[rotation_index + 2])[0])
            except:
                rotation = 0
            return video_fps, rotation
        return video_fps

    def extract_frames_from_video(self, frames_per_sec):
        """extracts and saves frames from input video file.

        Arguments:
            frames_per_sec {int} -- number of frames to be extracted in a sec.

        Returns
        {int} -- number of frames extracted from the video
        """


        if frames_per_sec == "all":
            frames_per_sec = self.video_fps
        cmd = "ffmpeg -loglevel panic -i {0} -vf fps={1}/1 -q:v 1 -qmin 1 -qmax 1 {2}/$filename%09d.jpg".format(self.video_path,frames_per_sec,self.frames_folder)
        os.system(cmd)
        return float(len(glob.glob(self.frames_folder+"/*.jpg")))

    def create_batches(self,batch_size):
        """Creates groups of extracted images as per given batch size.

        Arguments:
            batch_size {int} -- number specifying the size of the batch to be created

        Return:
            image_paths_batch {list} -- list containing groups of absolute image paths for roi creation and predictions
            -1 {int} -- integer depicting no frames present in frames folder
        """
        image_paths = glob.glob(self.frames_folder+"/*.jpg")
        image_paths_batch = list()
        batch_start = 0

        if len(image_paths) == 0:
            return -1

        for i in range(batch_size, len(image_paths), batch_size + 1):
            batch_end = i
            image_paths_batch.append(image_paths[batch_start:batch_end])
            batch_start = i
        image_paths_batch.append(image_paths[batch_start:len(image_paths)])
        return image_paths_batch

    def delete_video_folder(self):
        """deletes the folder containing all the intermediate processing files.
        """
        if os.path.exists(self.current_video_folder):
            import subprocess
            subprocess.call('rm -rf '+str(self.current_video_folder),shell=True)
