import datetime
import json
import threading
import time

import config
import pika
from MessageBroker import MessageBrokerFactory
from service.thumbnailService import ThumbnailService
from util import Log


class Task():
    host = config.QUEUE_HOST
    userName = config.QUEUE_USER_NAME
    password = config.QUEUE_PASSWORD
    port = config.QUEUE_PORT

    def publish(self, data, queue, routeKey, exchangeName, exchangeType):
        Broker = self.createBroker(queue, exchangeName, exchangeType)
        Broker.queue_bind(queue, exchangeName, routeKey)
        Broker.publish(exchangeName, routeKey, data)
        Broker.close_connection()

    def subscribe(self):
        queue = config.SUBSCRIBE_QUEUE_NAME
        exchangeName = config.SUBSCRIBE_QUEUE_EXCHANGE_NAME
        exchangeType = config.SUBSCRIBE_QUEUE_EXCHANGE_TYPE
        exchangeKey = config.SUBSCRIBE_ROUTE_KEY
        Broker = self.createBroker(queue, exchangeName, exchangeType)
        Broker.queue_bind(queue, exchangeName, exchangeKey)
        Broker.subscribe(exchangeName, queue, self.callback)

    def createBroker(self, queue, exchange, exchangeType):
        subscribeBroker = MessageBrokerFactory()
        Broker = subscribeBroker.get_broker('rabbitmq')
        connection = Broker.build_broker(self.host, self.port, self.userName, self.password)
        Broker.open_connection(connection)
        Broker.declare_queue(queue)
        Broker.declare_exchange(exchange, exchangeType)
        return Broker

    def callback(self, channel, method, properties, body):
        try:
            data = json.loads(body)
            # this solves the issue of pikka time out when the process takes more than 180 sec
            thread = threading.Thread(target=self.process_data, args=(data,))
            thread.start()
            while thread.is_alive():  # Loop while the thread is processing
                channel._connection.sleep(1.0)
        except ValueError:
            Log.info("000", Log.LEVEL_ERROR, "Invalid json: " + body)
        except Exception as e:
            Log.info(data.get("_id"), Log.LEVEL_ERROR, str(e))
            errorData = self.process_error(data, str(e))
            self.publish(errorData, config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME,
                         config.ERROR_QUEUE_EXCHANGE_TYPE)
        finally:
            channel.basic_ack(delivery_tag=method.delivery_tag)

    def process_output(self, output, data):
        if output is not None:
            self.publish(data, config.PUBLISH_QUEUE_NAME, config.PUBLISH_ROUTE_KEY, config.PUBLISH_QUEUE_EXCHANGE_NAME,
                         config.PUBLISH_QUEUE_EXCHANGE_TYPE)
        else:
            error = "thumbnail not generated"
            Log.info(data.get("_id"), Log.LEVEL_ERROR, error)
            errorData = self.process_error(data, error)
            self.publish(errorData, config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME,
                         config.ERROR_QUEUE_EXCHANGE_TYPE)

    def process_data(self, data):
        try:

            start_time = time.time()
            video = data["video_url"]
            object_id = data.get('_id')

            Log.info(object_id, Log.LEVEL_INPUT, video)

            if data.get("process_thumbnail") is True:

                output = ThumbnailService.generateThumbnail(video, object_id)
                data['thumbnail_image_url'] = output['image_path']

                Log.info(data.get("_id"), Log.LEVEL_OUTPUT, "Done")

                data['thumbnail_Analytics'] = float(time.time() - start_time)
                jdata = json.dumps(data)

                self.process_output(output, jdata)

            else:
                raise Exception("process_thumbnail is False")

        except Exception as e:
            Log.info(data.get("_id"), Log.LEVEL_ERROR, str(e))
            errorData = self.process_error(data, str(e))
            self.publish(errorData, config.ERROR_QUEUE_NAME, config.ERROR_ROUTE_KEY, config.ERROR_QUEUE_EXCHANGE_NAME,
                         config.ERROR_QUEUE_EXCHANGE_TYPE)

    def process_error(self, data, error):
        currentDT = datetime.datetime.now()
        data['errorTime'] = currentDT.strftime("%Y-%m-%d %H:%M:%S")
        data['api'] = config.APP_NAME
        data['error'] = error
        return json.dumps(data)

    def test(self):
        video = "https://d3i3lk5iax7dja.cloudfront.net/youplus.4buuyy8lssfqs1l2bejq.mp4"
        # video = "https://dvrevj1nlfko5.cloudfront.net/8da0d16d-c6d3-472e-8bd9-5c5e85278bb7.mp4"
        data = {"_id": "5d0cfc2523af17110e45c9f3",
                "crawledAt": 1562154035, "studyID": "5d0cf17259af316b3040b90a",
                "moduleType": "study", "downloadDuration": 0, "category": "", "accuracy": 0,
                "video_url": "https://d3i3lk5iax7dja.cloudfront.net/youplus.4buuyy8lssfqs1l2bejq.mp4"}
        object_id = data.get('_id')
        output = ThumbnailService.generateThumbnail(video, object_id)
        print(output)


if __name__ == '__main__':
    task = Task()
    print("running task")
    # task.test()
    try:
        task.subscribe()
    except pika.exceptions.StreamLostError:
        time.sleep(2 * 60)
        task.subscribe()
