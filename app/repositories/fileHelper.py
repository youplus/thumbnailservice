import urllib.request as urlRequest
import os
import re
import boto3
import botocore
from boto3.s3.transfer import S3Transfer

class FileHelper:

    currentPath = os.path.dirname(os.path.realpath(__file__))
    parentPath = os.path.abspath(os.path.join(currentPath, os.pardir))
    temp_dir = parentPath + '/Downloads/'

    
    def __init__(self):
        pass
    
    def file_Name(self, url):
        file_name = url.split('/')[-1]
        return file_name
    
    def save_file_to_local(self,file):
        file.save( self.temp_dir+ file.filename)
        return file.filename

    def download_File(self, url):
        # prepare save dir and filename        
        if not os.path.exists(self.temp_dir):
            os.mkdir(self.temp_dir)
        try:
            fileDirName = self.temp_dir + self.file_Name(url)
            urlRequest.urlretrieve(url, fileDirName)
        except Exception as e:
            raise Exception('download error with exception' + str(e))
    
    def delet_file(self, name):
        if os.path.exists(name):
            os.remove(name)
    
    def download_From_S3(self, file_name, bucket, key, credentials):
        s3 = boto3.resource('s3', **credentials)
        BUCKET_NAME = bucket
        KEY = key
        try:
            s3.Bucket(BUCKET_NAME).download_file( KEY + file_name, self.temp_dir + file_name )
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                raise Exception('file not found')
            else:
                raise
    
    def upload_To_S3(self, file_name, bucket, credentials ):
        client = boto3.client('s3', **credentials)
        transfer = S3Transfer(client)
        import mimetypes 
        mimetype, _ = mimetypes.guess_type(file_name)
        transfer.upload_file( self.temp_dir + file_name, bucket, file_name,extra_args={'CacheControl':'max-age=31536000','ContentType':mimetype
               })
        file_url = 'https://s3.ap-southeast-1.amazonaws.com'+'/%s/%s' % (bucket,file_name)
        return file_url
    
    def check_if_exists_s3(self, file_name, bucket, credentials):
        s3 = boto3.resource('s3', **credentials)
        BUCKET_NAME = bucket
        key = file_name
        objs = list(s3.Bucket(BUCKET_NAME).objects.filter(Prefix=key))
        if len(objs) > 0 and objs[0].key == key:
            return True
        else:
            return False
            
    def strip_special_char(self,title):
        title = re.sub('\W+','',title)
        return title
        