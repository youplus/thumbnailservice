import cv2
import math
import os 
import urllib.request
import glob
import requests 
import config
from repositories import predictionObject
from util import VideoProcessing
import time
import shutil
from shutil import copy
import numpy as np
from PIL import Image
from resizeimage import resizeimage
from .fileHelper import FileHelper


class Thumbnail:

    currentPath = os.path.dirname(os.path.realpath(__file__))
    parentPath = os.path.abspath(os.path.join(currentPath, os.pardir))
    temp_dir = parentPath + '/Downloads/'
    
    def __init__(self, url):
        self.url = url
        self.fileName = self.temp_dir + self.download_File()
        self.name = url.split('/')[-1].replace(".mp4","")
        
    
    def download_File(self):
        # prepare save dir and filename        
        if not os.path.exists(self.temp_dir):
            os.mkdir(self.temp_dir)
        file_name = self.url.split('/')[-1]
        if os.path.exists(self.temp_dir + file_name):
            return file_name
        urllib.request.urlretrieve(self.url, self.temp_dir + file_name)
        return file_name
    
    def deleteAllFiles(self):
        shutil.rmtree(self.temp_dir)
    
    def generate_thumbnail(self, id):
        output = self.get_nima_score(id ,self.fileName , predictionObject)
        return output

    def generate_min_nor_image(self, image, imageName):
        with Image.open(image) as image:
            firstStr = imageName.replace(".png","").replace("_original","")
            ext = ".png"
            minName = firstStr + "_min" + ext
            norName = firstStr + "_nor" + ext
            actualName = firstStr + ext

            min = resizeimage.resize_cover(image, [40, 30])
            min.save(self.temp_dir+ minName, image.format)

            actual = image.resize([1440, 500])
            actual.save(self.temp_dir+ actualName, image.format)
            
            nor = resizeimage.resize_cover(image, [400, 300])
            nor.save(self.temp_dir+ norName, image.format)

            return [  minName, norName, actualName]

    
    def get_nima_score(self, id, video_path, prediction_object, fps_to_process = 3):
        """Initiates thumbnail extraction process on a video.

        Arguments:
        video_path {str} -- absolute path to the input video file.
        prediction_object {class-object} -- object of class nima_prediction
        fps_to_process {int} -- number of frames to be used for expression calculations

        Return:
        final_json {dict} -- dictionary containing thumbnail_path, nima-aesthetic-technical scores
        -1 {int} -- in-case any of the required file doesn't exist or frames are not generated

        Assumption: model is already loaded
        """
        # initialize variables
        batch_size_for_prediction = 8

        # check if paths exist
        # check video path
        if not os.path.exists(video_path): return -1;

        # creating video editing object
        video_pro_object = VideoProcessing(video_path)

        # extract frames from video
        number_of_extracted_frames = video_pro_object.extract_frames_from_video(frames_per_sec=fps_to_process)

        # create_image_batches
        image_batches_to_process = video_pro_object.create_batches(batch_size=batch_size_for_prediction)
        if image_batches_to_process == -1: return -1


        # do nima prediction
        # nima initialisations
        total_predicted_image_paths = []
        nima_score_predictions = []
        aesthetic_score_predictions = []
        technical_score_predictions = []
        start2 = time.time()
        # nima prediction at batch level
        for batch in image_batches_to_process:
            predicted_image_paths, nima_scores, aesthetic_scores, technical_scores = prediction_object.predict_on_batch(batch)
            total_predicted_image_paths.extend(predicted_image_paths)
            nima_score_predictions.extend(nima_scores)
            aesthetic_score_predictions.extend(aesthetic_scores)
            technical_score_predictions.extend(technical_scores)

        ########################### TEMPORARY CODE ############################################
        # print("Number of extracted frames {}".format(number_of_extracted_frames))
        # print("Prediction time: {}".format(time.time()-start2))
        #######################################################################################

        #fetching the best thumbnail as per nima scores
        highest_nima_score_index = np.argmax(np.array(nima_score_predictions))
        highest_nima_score_image_path = (total_predicted_image_paths[highest_nima_score_index])
        highest_nima_score = (nima_score_predictions[highest_nima_score_index])
        highest_aesthetic_score = (aesthetic_score_predictions[highest_nima_score_index])
        highest_technical_score = (technical_score_predictions[highest_nima_score_index])
        # print(video_path)
        # saving the thumbnail in same folder as video
        # print(highest_nima_score_image_path)
        video_folder, video_name = os.path.split(video_path)
        quality_img_name = video_name.split(".mp4")[0] + id +"_original.png"
        quality_img_path = os.path.join(video_folder, quality_img_name)
        copy(highest_nima_score_image_path, quality_img_path)
        toUpload = self.generate_min_nor_image(quality_img_path,quality_img_name)
        toUpload.append(quality_img_name)
        # deleting all intermediate files
        # video_pro_object.delete_video_folder()
        uploaded_urls= self.upload_images_to_s3(toUpload)
        final_json = {
            "image_path": quality_img_path,
            "scores": {
                "nima":highest_nima_score,
                "aesthetic_score":highest_aesthetic_score,
                "technical_score":highest_technical_score
            }
        }
        final_path={'image_path':uploaded_urls}
        return final_path
    
    def upload_images_to_s3(self,toUpload):
        helper = FileHelper()
        credentials = { 
            'aws_access_key_id': config.THUMBNAILS_AWS_ACESS_KEY ,
            'aws_secret_access_key': config.THUMBNAILS_AWS_SECRET_KEY
        }
        bucket = config.UPLOAD_BUCKET
        # key = config.UPLOAD_KEY
        uploaded_urls = []
        for i in toUpload:
            file_name = i
            file_url = helper.upload_To_S3(file_name, bucket, credentials)
            exists = helper.check_if_exists_s3(file_name, bucket, credentials)
            if not exists:
                file_url = "https://s3-ap-southeast-1.amazonaws.com/ind-ypassets/logos/default_thumbnail.png"
            uploaded_urls.append(file_url)
        self.deleteAllFiles()
        # print(uploaded_urls)
        return uploaded_urls[2]
