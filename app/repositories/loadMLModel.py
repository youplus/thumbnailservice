import os 
from util import ThumbnailPrediction
import tensorflow as tf
class MlModel():
    def __init__(self):
        pass
    
    def loadModel(self):
        currentPath = os.path.dirname(os.path.realpath(__file__))
        parentPath = os.path.abspath(os.path.join(currentPath, os.pardir))
        modelPath = parentPath +"/MLModel/"
        # check aesthetic model path
        path_to_aesthetic_model = modelPath +"weights_mobilenet_aesthetic.hdf5"

        # check technical model path
        path_to_technical_model = modelPath +"weights_mobilenet_technical.hdf5"

        # creating thumbnail_prediction object
        graph = tf.get_default_graph()
        prediction_object = ThumbnailPrediction(path_to_aesthetic_model, path_to_technical_model, graph)

        return prediction_object

model = MlModel()

predictionObject = model.loadModel()

