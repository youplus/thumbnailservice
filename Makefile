install:
	docker-compose run --rm server pip install -r requirements.txt --user --upgrade --no-warn-script-location

start:
	docker-compose up server

prod-start:
	docker-compose up ProductonServe

coverage:
	docker-compose run --rm testserver bash -c "python -m pytest --cov-report term --cov-report html:coverage --cov-config setup.cfg --cov=src/ test/"

daemon:
	docker-compose up -d server

test:
	docker-compose run --rm testserver

lint:
	docker-compose run --rm server bash -c "python -m flake8 ./src ./test"

safety:
	docker-compose run --rm server bash -c "python vendor/bin/safety check"


build:
	docker build -t thumbnail-api .

k-prod:
	docker kill thumbnail-prod

r-prod:
	docker rm thumbnail-prod

prod:
	docker run -d --name thumbnail-prod --restart=unless-stopped --env-file='prod.env' -p 3008:3000 thumbnail-api

staging:
	docker run -d --name thumbnail-staging --restart=unless-stopped --env-file='staging.env' -p 3008:3000 thumbnail-api

k-stag:
	docker kill thumbnail-staging

r-stag:
	docker rm thumbnail-staging

